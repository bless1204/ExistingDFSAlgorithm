var mysql = require('mysql');

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'crawler',
  password: 'crawler',
  database: 'EXISTING_WEB_CRAWLER'
});

module.exports = {

  getConnection: function() {
    return connection;
  },

  startConnection: function(conn) {
    conn.connect(function(err){
      if(err) {
        console.log('An Error occurred while connecting to database...');
        console.log(err);
      } else {
        console.log('Database connection successful!');
      }
    });
  },

  endConnection: function(conn) {
    console.log('Closing MYSQL Connection');
    conn.end();
  }

};