var _ = require('underscore');
var request = require('request');
var cheerio = require('cheerio');
var htmlToText = require('html-to-text');
var sanitize = require('sanitize-html');
var url = require('url');
var req = require('rekuire');

var query = req('QueryHelper');

var pagesToVisit = [];
var data = [];
var maxDepth = 0;
var startTime, endTime, collectionType;

module.exports = {

  doWebCrawl: function(data, config, callback) {
    var frontiers = data.getFrontier;
    for (var f = 0; f < frontiers.length; f++) {
      pagesToVisit.push({
        id: frontiers[f].FRONTIER_ID,
        url: frontiers[f].URL,
        depth: 0
      });
    }
    maxDepth = config.maxDepth;
    collectionType = config.urlCollectionType;
    startTime = Date.now();
    crawl(callback);
  },

  saveCrawledData: function(data, callback) {
    query.insertMultipleData(data, function(error, conn){
      if(error) {
        console.log(error);
        callback(error);
      } else {
        console.log('All data are saved.');
        callback(null, conn);
      }
    });
  }
};

function crawl(callback) {
  if(pagesToVisit.length === 0) {
    endTime = Date.now();
    var time = {
      startTime: startTime,
      endTime: endTime
    };
    var result = { data: data, time: time };
    console.log('End of crawling...');
    callback(null, result);
  } else {
    var pageToVisit = pagesToVisit.pop();
    var pageDepth = pageToVisit.depth;
    /** Checks if Url is already visited **/
    visitPage(pageToVisit, callback);
  }
}

function visitPage(pageToVisit, callback) {
  var url = pageToVisit.url;
  console.log('Visiting page: ' + url);
  /** GET request to URL **/
  request(url, function(err, response, body){
    if(err) {
      console.log('An error occurred while visiting page.. PAGE: ' + url);
      console.log(err);
    } else {
      collectAndPushPageLinks(body, pageToVisit);
    }
    crawl(callback);
  });
}

function collectAndPushPageLinks(body, pageToVisit) {
  var $ = cheerio.load(body);
  var links = $('a');
  /** Gets only specific tags **/
  var sanitizedBody = sanitize($('html > body'));
  /** Converts HTML to text**/
  var htmlText = htmlToText.fromString(sanitizedBody, {
    wordwrap: false,
    ignoreHref: true,
    ignoreImage: true
  });

  /** Any data pushed here will be added to db **/
  data.push({
    frontierId: pageToVisit.id,
    url: pageToVisit.url,
    depth: pageToVisit.depth,
    title: $('title').text(),
    body: htmlText
  });

  console.log('pagesToVisit length: ' + pagesToVisit.length + ' depth: ' + pageToVisit.depth);

  /** Checks if maximum depth is already reached **/
  if(pageToVisit.depth < maxDepth) {
    for(var l=0; l<links.length; l++) {
      var href = links[l].attribs.href;
      if(!href) {
        continue;
      } 

      /** Get both relative and absolute links **/
      if(collectionType == 1 && (href.indexOf('http') === 0 || href[0] == '/')) {
        var href = href[0] == '/' ? url.resolve(pageToVisit.url, href) : href;
        pagesToVisit.push({
          id: pageToVisit.id,
          url: trimLast(href),
          depth: pageToVisit.depth + 1
        }); 
      } else if (collectionType == 2 && (href[0] == '/' || href.indexOf(pageToVisit.url) === 0)) {
        /** Get only relative links **/     
        var href = url.resolve(pageToVisit.url, href);
        pagesToVisit.push({
          id: pageToVisit.id,
          url: url.resolve(pageToVisit.url, trimLast(href)),
          depth: pageToVisit.depth + 1
        });
      } else if (collectionType == 3 && (href.indexOf('http') === 0)){
        /** Get only absolute links **/
        pagesToVisit.push({
          id: pageToVisit.id,
          url: href,
          depth: pageToVisit.depth + 1
        });
      }
    }
  }
}

function trimLast(href) {
  if(href[href.length-1] === '/' || href[href.length-1] === '#') {
    href = href.slice(0, -1);
  }
  return href;
}