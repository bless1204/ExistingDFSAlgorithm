RUNNING INSTRUCTIONS:

---> Initialize Database
1. Create new Database with name: EXISTING_WEB_CRAWLER
2. Import or Run SQL/initializeDatabase.sql on MYSQL editor
3. Change username, password, & host on config/database.js
4. INSERT URL/s to FRONTIER table
    INSERT INTO FRONTIER(URL, TITLE) VALUES ('http://stackoverflow.com', 'StackOverflow');

---> Run node by:
  node dfsAlgorithm.js [-depth] [-typeOfUrl]

  typeOfUrl:
    1 - Absolute & Relative links
    2 - Relative Links
    3 - Absolute Links

  e.g
  - maximum depth of 2, get only relative links
    node dfsAlgorithm.js 2 2
  - maximum depth of 3, get both absolute and relative links
    node dfsAlgorithm.js 3 1
  - maximum depth of 1, get only absolute links
    node dfsAlgorithm.js 1 3

===================== ALGORITHM ====================
PROCESS (WEBCRAWLING - DFS)
1. Define maximum depth
2. Initialize Frontier (Get from the database)
3. Add Frontier URL/s to pagesToBeVisited
4. Do Web Crawl
   4.1 Check if PagesToBeVisited is empty, if empty go to 5, else 4.2
   4.2 Pop a Url from pagesToBeVisited
   4.3 Visit popped Url 
   4.4 Add url to visitedPages
   4.5 Get Page Body
   4.6 Add to PagesToBeIndexed
   4.7 Check if maximum depth is reached if no, 4.8 else go to 4.1
   4.8 Get all Url from page
   4.9 Push url/s to pagesToBeVisited
5. Save PagesToBeIndexed to database

=================== SEARCH ENGINE ==================
PROCESS
1. Submit word/s or phrase to be searched
2. Retrieve related index data from database
3. Return most matched from database