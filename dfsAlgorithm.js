var req = require('rekuire');
var async = require('async');

var v = req('Validator');
var wc = req('WebCrawler');
var query = req('QueryHelper');

var db = req('database');

/* Parameters:
 * [0] - node
 * [1] - filename
 * [2] - depth
 * [3] - urlCollectionType 
 *      (1 - relative & absolute links, 2 - relative links, 3 - absolute links)
 */

var config = {
  maxDepth: process.argv[2] || 1,
  urlCollectionType: process.argv[3] || 1
};

(function(){
  async.auto({
    getFrontier: function(asyncCb) {
      query.getFrontier(asyncCb);
    },
    doWebCrawl: ['getFrontier', function(asyncCb, data) {
      wc.doWebCrawl(data, config, asyncCb);
    }],
    saveCrawledData: ['doWebCrawl', function(asyncCb, data) {
      wc.saveCrawledData(data.doWebCrawl.data, asyncCb);
    }],
    insertTimeStamps: ['saveCrawledData', function(asyncCb, data) {
      query.insertTimeStamps(asyncCb, data.doWebCrawl.time);
    }]
  }, function(error, result){
    db.endConnection(result.saveCrawledData);
    console.log('done!');
  });
})();
